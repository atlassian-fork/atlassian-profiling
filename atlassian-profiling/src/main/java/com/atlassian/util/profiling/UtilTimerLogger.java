package com.atlassian.util.profiling;

/**
 * Allows {@link com.atlassian.util.profiling.strategy.impl.StackProfilingStrategy#setLogger plugging} a
 * custom logger into {@link UtilTimerStack}.
 *
 * @deprecated in 3.0 for removal in 4.0
 */
@Deprecated
public interface UtilTimerLogger {

    /**
     * Writes a string to the logger
     *
     * @param s the string to write
     */
    void log(String s);
}
