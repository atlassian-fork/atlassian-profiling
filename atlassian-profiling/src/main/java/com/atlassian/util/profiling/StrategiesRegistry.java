package com.atlassian.util.profiling;

import com.atlassian.util.profiling.strategy.MetricStrategy;
import com.atlassian.util.profiling.strategy.ProfilerStrategy;
import com.atlassian.util.profiling.strategy.impl.StackProfilerStrategy;

import javax.annotation.Nonnull;
import java.util.Collection;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.concurrent.CopyOnWriteArrayList;

import static java.util.Objects.requireNonNull;

/**
 * @since 3.0
 */
public class StrategiesRegistry {

    private static final List<MetricStrategy> metricStrategies;
    private static final List<ProfilerStrategy> profilerStrategies;

    static {
        metricStrategies = new CopyOnWriteArrayList<>();
        profilerStrategies = new CopyOnWriteArrayList<>();
        // add the default StackProfilingStrategy as the first item in the list
        StackProfilerStrategy defaultStrategy = new StackProfilerStrategy();
        defaultStrategy.setConfiguration(Timers.getConfiguration());
        profilerStrategies.add(defaultStrategy);
    }

    private StrategiesRegistry() {
        throw new IllegalStateException("StrategiesRegistry should not be instantiated");
    }

    public static void addMetricStrategy(@Nonnull MetricStrategy strategy) {
        metricStrategies.add(requireNonNull(strategy, "strategy"));
        strategy.setConfiguration(Metrics.getConfiguration());
    }

    public static void addProfilerStrategy(@Nonnull ProfilerStrategy strategy) {
        profilerStrategies.add(requireNonNull(strategy, "strategy"));
        strategy.setConfiguration(Timers.getConfiguration());
    }

    public static boolean removeMetricStrategy(@Nonnull MetricStrategy strategy) {
        return metricStrategies.remove(strategy);
    }

    public static boolean removeProfilerStrategy(@Nonnull ProfilerStrategy strategy) {
        if (Objects.equals(strategy, getDefaultProfilerStrategy())) {
            // never allow the default profiling strategy to be removed
            return false;
        }
        return profilerStrategies.remove(strategy);
    }

    @Nonnull
    public static StackProfilerStrategy getDefaultProfilerStrategy() {
        return (StackProfilerStrategy) profilerStrategies.get(0);
    }

    @Nonnull
    static Collection<MetricStrategy> getMetricStrategies() {
        return Collections.unmodifiableCollection(metricStrategies);
    }

    @Nonnull
    static Collection<ProfilerStrategy> getProfilerStrategies() {
        return Collections.unmodifiableCollection(profilerStrategies);
    }
}
