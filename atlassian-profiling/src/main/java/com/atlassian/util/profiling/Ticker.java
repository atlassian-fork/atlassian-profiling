package com.atlassian.util.profiling;

/**
 * An object used to profile and/or monitor of a code block, that can be used to {@link #close() mark} the end of the
 * code block. {@code Ticker} extends {@link AutoCloseable} to allow for try-with-resources style usage:
 *
 * <pre>
 *     try (Ticker ignored = Timers.start("my-timer")) {
 *         // monitored code block here
 *     }
 * </pre>
 *
 * or
 *
 * <pre>
 *     try (Ticker ignored = Metrics.start("my-metric")) {
 *         // monitored code block here
 *     }
 * </pre>
 *
 * @since 3.0
 */
public interface Ticker extends AutoCloseable {

    /**
     * Simple no-op {@code Ticker} implementation
     */
    Ticker NO_OP = () -> {
    };

    // Overridden to remove the exception
    @Override
    void close();
}