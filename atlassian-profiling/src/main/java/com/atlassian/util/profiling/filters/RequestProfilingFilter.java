package com.atlassian.util.profiling.filters;

import com.atlassian.util.profiling.Ticker;
import com.atlassian.util.profiling.Timer;
import com.atlassian.util.profiling.Timers;

import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import java.io.IOException;

/**
 * Filter that ensures {@link Timer timer} state is cleaned up at the
 * {@link Timers#onRequestEnd() end of the request}.
 * <p>
 * Optionally, the filter can be configured to allow per-request profiling when a predefined request parameter is
 * present on the request. The name of this parameter can be configured through the {@code activate.param} filter
 * init-param:
 * <pre>
 *   &lt;filter&gt;
 *      &lt;filter-name&gt;profiling&lt;/filter-name&gt;
 *      &lt;filter-class&gt;com.atlassian.util.profiling.filters.RequestProfilingFilter&lt;/filter-class&gt;
 *      &lt;init-param&gt;
 *             &lt;param-name&gt;activate.param&lt;/param-name&gt;
 *             &lt;param-value&gt;profilingfilter&lt;/param-value&gt;
 *         &lt;/init-param&gt;
 *  &lt;/filter&gt;
 * </pre>
 *
 * If {@code activate.param} is not defined, per-request profiling will not be supported.
 *
 * @since 3.0
 */
public class RequestProfilingFilter implements Filter {

    /**
     * This is the parameter you pass to the init parameter &amp; specify in the web.xml file: eg.
     * <pre>
     * &lt;filter&gt;
     *   &lt;filter-name&gt;profile&lt;/filter-name&gt;
     *   &lt;filter-class&gt;com.atlassian.util.profiling.filters.RequestProfilingFilter&lt;/filter-class&gt;
     *  &lt;init-param&gt;
     *    &lt;param-name&gt;activate.param&lt;/param-name&gt;
     *    &lt;param-value&gt;request_profiling&lt;/param-value&gt;
     *  &lt;/init-param&gt;
     * &lt;/filter&gt;
     *  </pre>
     */
    private static final String ACTIVATE_PARAM = "activate.param";

    private String activateParameterName;

    @Override
    public void destroy() {
    }

    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
            throws IOException, ServletException {

        try {
            if (activateParameterName == null || request.getParameter(activateParameterName) == null) {
                chain.doFilter(request, response);
            } else {
                String timerName = ((HttpServletRequest) request).getRequestURI();
                try (Ticker ignored = Timers.getConfiguration().enableForThread();
                     Ticker ignored2 = Timers.start(timerName)) {
                    chain.doFilter(request, response);
                }
            }
        } finally {
            Timers.onRequestEnd();
        }
    }

    @Override
    public void init(FilterConfig filterConfig) {
        //some servlet containers set this to be null
        if (filterConfig != null) {
            String param = filterConfig.getInitParameter(ACTIVATE_PARAM);
            if (param != null && !param.trim().isEmpty()) {
                activateParameterName = param.trim();
            }
        }
    }
}
