package com.atlassian.util.profiling.filters;

import javax.servlet.ServletRequest;

/**
 * @deprecated in 3.0 for removal in 4.0
 */
public interface StatusUpdateStrategy {
    /**
     * Turn on or off profiling via the request.
     *
     * @param request containing the query string to parse
     */
    void setStateViaRequest(ServletRequest request);
}
