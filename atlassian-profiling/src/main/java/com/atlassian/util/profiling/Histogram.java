package com.atlassian.util.profiling;

/**
 * A metric that calculates the distribution of a value

 * <pre>
 *     Histogram metric = Metrics.histogram("my-metric");
 *     timer.update(3);
 * </pre>

 * @since 3.0
 */
public interface Histogram {

    /**
     * Records a new value
     *
     * @param value the value to record
     */
    void update(long value);
}
