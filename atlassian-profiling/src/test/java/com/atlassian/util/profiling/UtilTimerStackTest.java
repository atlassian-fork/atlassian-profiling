package com.atlassian.util.profiling;

import com.atlassian.util.profiling.strategy.impl.StackProfilingStrategy;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static com.atlassian.util.profiling.UtilTimerStack.getDefaultStrategy;
import static org.junit.Assert.assertEquals;

/**
 * @deprecated in 3.0 for removal in 4.0
 */
@Deprecated
public class UtilTimerStackTest {

    private boolean configuredActive;
    private UtilTimerLogger configuredLogger;
    private int configuredMaxFrames;
    private long configuredMinTime;
    private long configuredMinTotalTime;
    private TestableLogger logger;

    @Before
    public void setup() {
        configuredActive = UtilTimerStack.isActive();
        StackProfilingStrategy strategy = getDefaultStrategy();
        configuredMaxFrames = strategy.getMaxFrameCount();
        configuredMinTime = strategy.getMinTime();
        configuredMinTotalTime = strategy.getMinTotalTime();
        configuredLogger = strategy.getLogger();
        logger = new TestableLogger();

        strategy.setLogger(logger);
        strategy.setEnabled(true);
    }

    @After
    public void tearDown() {
        StackProfilingStrategy strategy = getDefaultStrategy();
        strategy.setConfiguredMaxFrameCount(configuredMaxFrames);
        strategy.setConfiguredMinTime(configuredMinTime);
        strategy.setConfiguredMinTotalTime(configuredMinTotalTime);
        strategy.setLogger(configuredLogger);
        strategy.setEnabled(configuredActive);
    }

    @Test
    public void testMaxFrameCount() throws InterruptedException {
        getDefaultStrategy().setConfiguredMaxFrameCount(5);

        UtilTimerStack.push("outer");
        for (int i = 0; i < 10; ++i) {
            UtilTimerStack.push("inner " + i);
            UtilTimerStack.push("inner-inner " + i);
            Thread.sleep(1L);
            UtilTimerStack.pop("inner-inner " + i);
            UtilTimerStack.pop("inner " + i);
        }
        UtilTimerStack.pop("outer");

        assertEquals(5, countLines(logger.getLog()));
    }

    @Test
    public void testMinTime() throws InterruptedException {
        getDefaultStrategy().setConfiguredMinTime(10);

        UtilTimerStack.push("outer");
        for (int i = 0; i < 6; ++i) {
            UtilTimerStack.push("inner " + i);
            UtilTimerStack.push("inner-inner " + i);
            Thread.sleep(i % 2 == 0 ? 15L : 1L);
            UtilTimerStack.pop("inner-inner " + i);
            if (i % 3 == 0) {
                Thread.sleep(15L);
            }
            UtilTimerStack.pop("inner " + i);
        }
        UtilTimerStack.pop("outer");

        // expecting logged lines for
        // - outer
        //   - inner 0
        //     - inner-inner 0
        //   - inner 2
        //     - inner-inner 2
        //   - inner 3
        //   - inner 4
        //     - inner-inner 4

        assertEquals(8, countLines(logger.getLog()));
    }

    private int countLines(String value) {
        if (value == null) {
            return 0;
        }
        return value.split("\n").length;
    }

    private static class TestableLogger implements UtilTimerLogger {
        private StringBuilder builder = new StringBuilder();

        @Override
        public void log(String s) {
            builder.append(s);
        }

        public String getLog() {
            return builder.toString();
        }
    }
}
