package com.atlassian.util.profiling.instrumentation;

import com.atlassian.instrumentation.DefaultInstrumentRegistry;
import com.atlassian.instrumentation.InstrumentRegistry;
import com.atlassian.instrumentation.operations.OpCounter;
import com.atlassian.util.profiling.Metrics;
import com.atlassian.util.profiling.MetricTimer;
import com.atlassian.util.profiling.MetricsConfiguration;
import com.atlassian.util.profiling.StrategiesRegistry;
import com.atlassian.util.profiling.Timer;
import com.atlassian.util.profiling.Timers;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import static org.junit.Assert.assertEquals;

public class InstrumentationMetricStrategyTest {

    private MetricsConfiguration configuration;
    private InstrumentRegistry registry;
    private InstrumentationMetricStrategy strategy;
    private boolean wasEnabled;

    @Before
    public void setup() {
        registry = new DefaultInstrumentRegistry();
        strategy = new InstrumentationMetricStrategy(registry);

        configuration = Metrics.getConfiguration();
        wasEnabled = configuration.isEnabled();
        configuration.setEnabled(true);
        StrategiesRegistry.addMetricStrategy(strategy);
    }

    @After
    public void tearDown() {
        StrategiesRegistry.removeMetricStrategy(strategy);
        configuration.setEnabled(wasEnabled);
    }

    @Test
    public void testDisabledMetricWorksWhenEnabled() {
        configuration.setEnabled(false);

        MetricTimer metric = Metrics.timer("my.metric");
        OpCounter opCounter = registry.pullOpCounter("my.metric");

        assertEquals(0L, opCounter.getInvocationCount());

        metric.start().close();

        assertEquals(0L, opCounter.getInvocationCount());

        configuration.setEnabled(true);
        metric.start().close();

        assertEquals(1L, opCounter.getInvocationCount());
    }

    @Test
    public void testEnabledMetricNoopsWhenDisabled() {
        MetricTimer metric = Metrics.timer("my.metric");
        OpCounter opCounter = registry.pullOpCounter("my.metric");

        assertEquals(0L, opCounter.getInvocationCount());

        // verify that the metric is enabled
        metric.start().close();
        assertEquals(1L, opCounter.getInvocationCount());

        // disable the strategy and verify that interacting with the metric does nothing
        configuration.setEnabled(false);
        metric.start().close();

        assertEquals(1L, opCounter.getInvocationCount());

        // re-enable and verify that the metric is enabled too
        configuration.setEnabled(true);
        metric.start().close();

        assertEquals(2L, opCounter.getInvocationCount());
    }

    @Test
    public void testMetric() {
        MetricTimer metric = Metrics.timer("test.metric");
        OpCounter opCounter = registry.pullOpCounter("test.metric");

        assertEquals(0L, opCounter.getInvocationCount());

        metric.start().close();
        assertEquals(1L, opCounter.getInvocationCount());

        metric.start().close();
        assertEquals(2L, opCounter.getInvocationCount());
    }

    @Test
    public void testTimerWithMetric() {
        Timer timer = Timers.timerWithMetric("test.metric");
        OpCounter opCounter = registry.pullOpCounter("test.metric");

        assertEquals(0L, opCounter.getInvocationCount());

        timer.start().close();
        assertEquals(1L, opCounter.getInvocationCount());
    }

    @Test
    public void testTimerWithMetricDifferentMetricName() {
        Timer timer = Timers.timerWithMetric("something something", "test.metric");
        OpCounter opCounter = registry.pullOpCounter("test.metric");

        assertEquals(0L, opCounter.getInvocationCount());

        timer.start().close();
        assertEquals(1L, opCounter.getInvocationCount());
    }
}