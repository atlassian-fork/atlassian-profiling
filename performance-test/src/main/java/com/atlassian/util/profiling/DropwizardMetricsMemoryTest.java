package com.atlassian.util.profiling;

import com.atlassian.util.profiling.dropwizard.DropwizardMetricStrategy;
import com.codahale.metrics.MetricRegistry;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;

public class DropwizardMetricsMemoryTest {

    private static class ExampleTask implements Runnable {

        private final CountDownLatch latch;

        private ExampleTask(CountDownLatch latch) {
            this.latch = latch;
        }

        @Override
        public void run() {
            try {
                while (!latch.await(0, TimeUnit.MILLISECONDS)) {
                    try (Ticker ignored = Timers.startWithMetric("outer metric")) {
                        for (int i = 0; i < 100; ++i) {
                            try (Ticker ignored2 = Timers.startWithMetric("inner metric "+ i)) {
                                Thread.sleep(2L);
                            }
                        }
                    }
                }
            } catch (InterruptedException e) {
                Thread.currentThread().interrupt();
            }
        }
    }

    /*
     * This test aims to make it easy to analyze performance and memory usage by attaching a profiler to a simple
     * program that executes atlassian-profiling logic (profiling and dropwizard metrics)
     *
     * This test starts 8 threads that execute profiling and metrics for 15 minutes.
     */
    public static void main(String[] args) throws InterruptedException {
        StrategiesRegistry.addMetricStrategy(new DropwizardMetricStrategy(new MetricRegistry()));
        Metrics.getConfiguration().setEnabled(true);
        Timers.getConfiguration().setEnabled(true);

        List<Thread> threads = new ArrayList<>();
        CountDownLatch latch = new CountDownLatch(1);
        for (int i = 0; i < 8; ++i) {
            Thread t = new Thread(new ExampleTask(latch));
            t.start();
            threads.add(t);
        }

        Thread.sleep(900000L);
        latch.countDown();
        for (Thread t : threads) {
            t.join();
        }
    }

}
